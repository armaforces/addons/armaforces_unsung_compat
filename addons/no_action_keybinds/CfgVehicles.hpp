
class CfgVehicles
{
    class Car_F;
    // uns_Wheeled_w_c
    class uns_M37_base: Car_F
    {
        class UserActions
        {
            class wipers_on;
            class PopSmokeP: wipers_on
            {
                shortcut = "";
            };
            class DropFlare: wipers_on
            {
                shortcut = "";
            };
        };
    };

    class Truck_F;
    // uns_m35_c
    class uns_M35A2_Base: Truck_F
    {
        class UserActions
        {
            class wipers_on;
            class PopSmokeP: wipers_on
            {
                shortcut="";
            };
            class DropFlare: PopSmokeP
            {
                shortcut="";
            };
        };
    };

    class Helicopter;
    // uns_uh1d_c
    class uns_UH1D_base: Helicopter
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    // CSJ_UH1Gun_c
    class uns_UH1C_M21_M200: Helicopter
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    // uns_oh6_c
    class uns_oh6_base: Helicopter
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    class Plane_Base_F;
    // uns_skymaster_c
    class uns_skymaster_base: Plane_Base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    class Plane;
    // uns_an2_c
    class uns_an2: Plane
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    class Helicopter_Base_F;
    // uns_H21C_c
    class uns_h21c_base: Helicopter_Base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    // uns_ch34_c
    class uns_ch34: Helicopter_Base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    // uns_ch47a_c
    class uns_ch47_m60_usmc: Helicopter_Base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    // uns_ch53_c
    class uns_ch53_base: Helicopter_Base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    // uns_mi8_c
    class uns_Mi8TV_base: Helicopter_Base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    class Heli_Light_01_base_F;
    // pook_H13_c
    class uns_H13_base: Heli_Light_01_base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade
            {
                shortcut = "";
            };
            class DropFlare
            {
                shortcut = "";
            };
        };
    };

    class Rubber_duck_base_F;
    // uns_boats_c
    class Zodiac: Rubber_duck_base_F
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    class Smallboat_1;
    // uns_boats_c
    class UNS_floatraft_3: Smallboat_1
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

    class StaticSEARCHLight;
    // uns_stabo
    class uns_stabo: StaticSEARCHLight
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "GetOver";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "opticsMode";
            };
        };
    };

    class Wheeled_APC_F;
    // uns_m113_c
    class uns_M113_base: Wheeled_APC_F
    {
        class UserActions
        {
            class OpenRear;
            class PopSmokeP: OpenRear
            {
                shortcut = "";
            };
            class DropFlare: OpenRear
            {
                shortcut = "";
            };
        };
    };

    class uns_M113_XM182;
    // uns_m113_c
    class uns_m163: uns_M113_XM182
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare
            {
                shortcut = "";
            };
        };
    };

    class uns_M113_M2;
    // uns_m113_c
    class uns_M113A1_M2: uns_M113_M2
    {
        class UserActions
        {
            class OpenRear;
            class PopSmokeP: OpenRear
            {
                shortcut = "";
            };
            class DropFlare: OpenRear
            {
                shortcut = "";
            };
        };
    };

    class UNS_UH1C_M3_ARA;
    // CSJ_UH1Gun_c
    class UNS_UH1B_TOW: UNS_UH1C_M3_ARA
    {
        class UserActions
        {
            class PopSmokeP
            {
                shortcut = "";
            };
            class ThroGrenade: PopSmokeP
            {
                shortcut = "";
            };
            class DropFlare: PopSmokeP
            {
                shortcut = "";
            };
        };
    };

};
