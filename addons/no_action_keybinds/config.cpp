class CfgPatches
{
	class armaforces_unsung_no_action_keybinds
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {
            "uns_Wheeled_w_c",
            "uns_m35_c",
            "uns_uh1d_c",
            "CSJ_UH1Gun_c",
            "uns_oh6_c",
            "uns_skymaster_c",
            "uns_an2_c",
            "uns_H21C_c",
            "uns_ch34_c",
            "uns_ch47a_c",
            "uns_ch53_c",
            "uns_mi8_c",
            "pook_H13_c",
            "uns_boats_c",
            "uns_stabo",
            "uns_m113_c"
        };
		version = 1.0;
		versionStr = 1.0;
		versionAr[] = {1,0};
		author = "veteran29";
	};
};

#include "CfgVehicles.hpp"
