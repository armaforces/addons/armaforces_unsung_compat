@echo off
setlocal EnableDelayedExpansion

echo                                        ______
echo        /\                             ^|  ____^|
echo       /  \    _ __  _ __ ___    __ _  ^| ^|__  ___   _ __  ___  ___  ___
echo      / /\ \  ^| '__^|^| '_ ` _ \  / _` ^| ^|  __^|/ _ \ ^| '__^|/ __^|/ _ \/ __^|
echo     / ____ \ ^| ^|   ^| ^| ^| ^| ^| ^|^| (_^| ^| ^| ^|  ^| (_) ^|^| ^|  ^| (__^|  __/\__ \
echo    /_/    \_\^|_^|   ^|_^| ^|_^| ^|_^| \__,_^| ^|_^|   \___/ ^|_^|   \___^|\___^|^|___/
echo.

set private_key=ArmaForces.biprivatekey
set mod=@armaforces_unsung_stuff\addons
if not exist "%mod%" mkdir "%mod%"

del /f /s /q %mod% 1>nul

echo ==== Build ====
for /D %%i in (.\addons\*) do (
    set pbo=armaforces_%%~ni.pbo
    echo [Building] !pbo! from %%i
    call .\tools\armake2.exe build -x *.tga -i P:\ "%%i" ".\%mod%\!pbo!"

    if errorlevel 1 (
        echo [!pbo!] Failed
    ) else (
        echo [!pbo!] Ok
    )
    echo.
)

if exist %private_key% (
    echo ==== Sign ====
    for %%p in (.\%mod%\*.pbo) do (
        echo [Signing] %%p
        call .\tools\armake2.exe sign .\%private_key% "%%p"

        if errorlevel 1 (
            echo Failed
        ) else (
            echo Ok
        )
        echo.
    )
) else (
    echo Key not found: %private_key%
    echo Skipping signing.
)

if errorlevel 1 (
    echo [Finished] There were some errors
    pause
) else (
    echo [Finished]
    REM pause
)
