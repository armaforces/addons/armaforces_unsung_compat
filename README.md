# ArmaForces - Unsung stuff

[Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1643338196)

## Zawartośc

 * unsung_no_action_keybinds - Usunięcie skrótów klawiszowych do granatów oraz flar syngalizacyjnych z pojazdów transportowych


## Środowisko deweloperskie

Zalecany edytor Visual Studio Code wraz z dodatkami:
 * [SQFLint](https://marketplace.visualstudio.com/items?itemName=skacekachna.sqflint) - Analiza oraz linting sqf
 * [psioniq File Header](https://marketplace.visualstudio.com/items?itemName=psioniq.psi-header) - Generowanie nagłówka plików

Do zbudowania addonów zalecane jest użycie [armake2](https://github.com/KoffeinFlummi/armake2).

Skompilowana wersja `armake2` jest dostępna w folderze tools (wersja x64).

Dla ułatwienia dewelopmentu w repozytorium znajduje się plik `build.bat` który buduje i podpisuje addon za pomocą dostarczonych do repozytorium narzędzi.  
Skrypt ten będzie próbował podpisać addony kluczem `ArmaForces.biprivatekey` który powinien znajdować się bezpośrednio w katalogu repozytorium.
